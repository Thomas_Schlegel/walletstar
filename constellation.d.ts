// Type definitions for Application Container API

declare function consolelog(message: string): string;

declare namespace container {
    interface CallbackFunc {
        (results: { success: boolean, message: string, data?: object }): void
    }

    namespace analytics {
        function logAction(actionText: string): void;

        function logPrimaryAction(actionText: string): void;
    }

    namespace connectors {
        function list(callbackFunc: CallbackFunc): void;

        function sendRequest(connname: string, connversion: string, connmethod: string, requestParams: Object, callbackFunc: Function): void;
    }

    namespace device {
        function getCapabilities(callbackFunc: (data: object) => void): void;

        function getLocation(callbackFunc: CallbackFunc): void;

        function openMap(lat: number, long: number, name: string): void;
    }

    function getImageUrl(filename: string, doneFunc: CallbackFunc): void;

    function getImageUrls(filnamesArray: string[], doneFunc: CallbackFunc): void;

    function getString(strCode: string): string;

    namespace imagesdk {
        interface ImageOptions {
            numimages?: number,
            orientation?: string,
            imagescale?: number,
            imagequality?: number,
            cameraprompts?: string[],
            pickerprompt?: string,
            controlsvisible?: { flashbutton?: boolean },
            colors?: { flashbutton?: string, takepicturebutton?: string }
        }

        function captureImage(options: ImageOptions, cameraOnly: boolean, callbackFunc: CallbackFunc): void;

        function captureBillStub(options: ImageOptions, callbackFunc: CallbackFunc): void;

        function captureDriversLicenseBack(options: ImageOptions, callbackFunc: CallbackFunc): void;

        function captureDriversLicenseFront(options: ImageOptions, callbackFunc: CallbackFunc): void;

        function captureDriversLicenseFrontBack(options: ImageOptions, callbackFunc: CallbackFunc): void;

        function captureReceipt(options: ImageOptions, callbackFunc: CallbackFunc): void;
    }

    function isAndroid(): boolean;

    function isAndroidApp(): boolean;

    function isIos(): boolean;

    function isIosApp(): boolean;

    function isNumeric(stringNum: string): boolean;

    function isOther(): boolean;

    function loadJsonFile(filename: string, doneFunc: CallbackFunc): void;

    function loadStrings(endFunc: Function): void;

    namespace mobileapps {
        function canOpen(urlSchemeOrPackageName: string, callbackFunc: CallbackFunc): boolean;

        function open(urlSchemeOrPackageName: string, callbackFunc: CallbackFunc): void;
    }

    function openWebLink(link: string): void;

    function performAjaxRequest(urlString: string, doneFunc: CallbackFunc, options?: {
        method: string, headers: object, qa: object, url_encoded_formpost: boolean
    }, body?: object): void;

    function setFocusWithDelay(idString: string, focusDelay?: number = 200): void;

    namespace tiles {
        function canOpenTile(tilecode: string, tileversion: string, callbackFunc: CallbackFunc): void;

        function getOpenData(callbackFunc: CallbackFunc): void;

        function openTile(tilecode: string, tileversion: string, callbackFunc: CallbackFunc, opendata?: object): void;
    }

    namespace ui {
        function getPushAnimation(): string;

        function hideSpinner(): void;

        function pushPanelWithTitle(onsnavigator: object, templatename: string, title: string, onsenoptions?: object): void;

        function setImgSrcWithEncodedJpegData(idString: string, encodedData: string): void;

        function setupAlerts(titleText: string, okText: string): void;

        function showAlert(message: string, returnFocus?: boolean, okFunc?: Function): void;

        function showBody(callback?: Function): void;

        function showFieldError($field: object, message: string): void;

        function showMessage(title: string, message: string, returnFocus?: boolean, okFunc?: Function): void;

        function showSpinner(message: string): void;

        function updatePanelWithStrings(pageId?: string): void;
    }
}
