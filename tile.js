
(function(){
    var tile = {

        // REQUIRED function - Used by tile.popPanel() to locate the ons-navigator instance
        // If you use a different ID for your ONS-NAVIGATOR, you will need to update this function
        getNav: function() {
            return $('#tilenav').get(0);
        },

        // REQUIRED function - called by the container when user presses Back button in the container
        popPanel: function() {
            tile.getNav().popPage();
        },

        // Recommended object structure to store string constants (makes your code more readable)
        // NOTE: You only need to add strings that are used in JavaScript and not every string in the strings file
        str: {
            ERR_GETPARTY_CALL_FAILED:                  	'100501',	// Errors
            ERR_GETACCOUNTS_CALL_FAILED:                '100502',

            ERR_GLOBAL_ERROR_DIALOG_TITLE:				'999001',
            ERR_GLOBAL_ERROR_DIALOG_BUTTON:				'999002'
        },

        data: {
            offerings: [],
            offerIndex: 0,
            accounts: [],
            accountsIndex: 0
        }
    };

    /*ons.ready(function() {

        var config = {
            apiKey: "AIzaSyCU4ltX8T3Uxavv6Wi91XjpEdjm2JNHkRo",
            authDomain: "constellation-demo-e741b.firebaseapp.com",
            databaseURL: "https://constellation-demo-e741b.firebaseio.com",
            projectId: "constellation-demo-e741b",
            storageBucket: "constellation-demo-e741b.appspot.com",
            messagingSenderId: "549866592235"
        };
        firebase.initializeApp(config);

    });*/

    $(document).on('ready', function () {
        var config = {
            apiKey: "AIzaSyCU4ltX8T3Uxavv6Wi91XjpEdjm2JNHkRo",
            authDomain: "constellation-demo-e741b.firebaseapp.com",
            databaseURL: "https://constellation-demo-e741b.firebaseio.com",
            projectId: "constellation-demo-e741b",
            storageBucket: "constellation-demo-e741b.appspot.com",
            messagingSenderId: "549866592235"
        };

        firebase.initializeApp(config);

        firebase.database().ref().once('value').then(function (snapshot) {
            var db = snapshot.val();
            tile.data.accounts = db.Accounts.slice();
            tile.data.offerings = db.Offerings.slice();

            for(var i = 0; i < tile.data.offerings.length; i++) {
                var offering = tile.data.offerings[i];
                $('<div class="item"><img src="'+ offering.Image +'"><div class="carousel-caption"></div>   </div>').appendTo('.carousel-inner');
                $('<li data-target="#carousel-example-generic" data-slide-to="'+i+'"></li>').appendTo('.carousel-indicators');
            }
            $('.item').first().addClass('active');
            $('.carousel-indicators > li').first().addClass('active');
            $('#Offers').carousel({});
            offeringCarouselChange(tile.data.offerIndex);
        });

        $(document).on('click', '#Right', function(){
            offeringCarouselChange(tile.data.offerIndex + 1);
        });

        $(document).on('click', '#Left', function(){
            offeringCarouselChange(tile.data.offerIndex - 1);
        });

        $(document).on('click', '#AccountsRight', function(){
            accountsCarouselChange(tile.data.accountsIndex + 1);
        });

        $(document).on('click', '#AccountsLeft', function(){
            accountsCarouselChange(tile.data.accountsIndex - 1);
        });


        $('#SelectCard').on('click', function(){
            var currentOffer = tile.data.offerings[tile.data.offerIndex];

            tile.data.accounts[0].Cards.push({
                'Balance': 2500,
                'Id': tile.data.accounts[0].Cards.length + 1,
                'Image': currentOffer.Image,
                'Type': currentOffer.Name,
                'Number': '1234-0987-4356-8222',
                'Transactions': []
            });
        });

    });

    function offeringCarouselChange(newIndex) {

        if(newIndex < 0){
            newIndex = tile.data.offerings.length - 1;
        }

        if(newIndex == tile.data.offerings.length){
            newIndex = 0;
        }

        var offeringRef = tile.data.offerings[newIndex];
        $('[data-card-description]').text(offeringRef.Description);
        $('[data-card-title]').text(offeringRef.Name);
        $('[data-card-purchase-apr]').text(offeringRef.PurchaseAPR);
        $('[data-card-intro-apr]').text(offeringRef.IntroductoryAPR);
        $('[data-card-bt-apr]').text(offeringRef.BalanceTransferAPR);
        $('#SelectCard').attr('data-id', newIndex);
        tile.data.offerIndex = newIndex;
    }

    function accountsCarouselChange(newIndex){
        var account = tile.data.accounts[0];
        if(newIndex < 0){
            newIndex = account.Cards.length - 1;
        }

        if(newIndex == account.Cards.length) {
            newIndex = 0;
        }
        var cardRef = account.Cards[newIndex];

        $('#Transactions').empty();

        cardRef.Transactions.foreach(transaction => {
            $('<ons-list-item data-id="' + transaction.Id + '"><div class="left"><img src="' + transaction.Image + '"/>' +
            '</div> <div class="center">' + transaction.Vendor + '</div> <div class="right"><ons-checkbox input-id="' + transaction.Id + '"></ons-checkbox>' +
            '</div> </ons-list-item>').appendTo('#Transactions');
        });

        tile.data.accountsIndex = newIndex;
    }

    document.addEventListener('init', function(event) {
        var page = event.target;

        firebase.database().ref().once('value').then(function(snapshot) {
            var db = snapshot.val();
            tile.data.accounts = db.Accounts.slice();
            tile.data.offerings = db.Offerings.slice();
        });
        // ************************************************************************
        // * Main page loaded
        // ************************************************************************
        if (page.pushedOptions.page === 'offers.html') {

            // container.loadStrings(function() {
            //
            //     container.ui.setupAlerts(tile.str.ERR_GLOBAL_ERROR_DIALOG_TITLE, tile.str.ERR_GLOBAL_ERROR_DIALOG_BUTTON);
            //
            //     $('#openpage2').click(function(e) {
            //         e.preventDefault();
            //         container.ui.pushPanelWithTitle(tile.getNav(), 'accounts.html', 'accounts');
            //     });
            //
            //     container.ui.updatePanelWithStrings();
            //     var ajaxOptions = {
            //         'method': 'POST'
            //     };
            //
            //     var body = {};
            //     var callBack = function(params){
            //
            //     };
            //     // Make the call to the external connector to get the party object
            //     container.performAjaxRequest('https://vision.googleapis.com/v1/images:annotate?key=',
            //         callback, ajaxOptions, body);
            // });
        }

        if (page.pushedOptions.page === 'accounts.html'){
            container.ui.updatePanelWithStrings('accounts');

        }
    });



})();
